package com.raju.toasterlibrary

import android.content.Context
import android.widget.Toast

public class Toaster {
    fun display(context: Context?, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}